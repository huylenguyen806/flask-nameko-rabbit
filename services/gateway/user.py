from gateway import app, rpc
from flask import request


@app.route('/user', methods=['GET', 'POST'])
def get_post_user():
    if request.method = 'GET':
        result = rpc.user_management.get()
    else:
        username = request.form.get('username')
        email = request.form.get('email')
        result = rpc.user_management.post(username=username, email=email)
    return result


@app.route('/user/<int:id>', methods=['GET', 'PUT', 'DELETE'])
def get_put_del_user(id):
    if request.method == 'GET':
        result = rpc.user_management.get(id)
    elif request.method == 'PUT':
        username = request.form.get('username')
        email = request.form.get('email')
        password = request.form.get('password')
        result = rpc.user_management.put(id, username, email, password)
    else:
        result = rpc.user_management.delete(id)
    return result
